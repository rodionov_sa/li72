from django.db import models
from django.db.models.signals import pre_save, post_save
from django.db.models import F
from django.db.models.functions import Coalesce
from django.utils.timezone import now
from django.urls import reverse

class Team(models.Model):
    name = models.CharField(max_length=100, unique=True)
    previous = models.ForeignKey('self', on_delete=models.SET_NULL, null=True, blank=True)
    class Meta:
        ordering = ['name']    
    def get_absolute_url(self):
        return reverse('team-detail', args=[str(self.id)])
    def __str__(self):
        return self.name


class Person(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    phone = models.CharField(max_length=10, null=True, blank=True, unique=True)
    vk_id = models.CharField(max_length=20, null=True, blank=True, unique=True)
    insta = models.CharField(max_length=100, null=True, blank=True, unique=True)
    last_team = models.ForeignKey('Team', on_delete=models.SET_NULL, null=True, blank=True)
    class Meta:
        ordering = ['last_name', 'first_name']
    '''
    @property
    def get_team(self):
        return Team.objects.filter(application__applicant=self).order_by('-application__date').first()
    #get_team.short_description = 'Last team'
    '''
    def get_absolute_url(self):
        return reverse('person-detail', args=[str(self.id)])
    def __str__(self):
        return self.last_name + ' ' + self.first_name + ', ' + str(self.phone)


class Location(models.Model):
    name = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    address = models.CharField(max_length=100)
    class Meta:
        ordering = ['city', 'name']    
    def get_absolute_url(self):
        return reverse('location-detail', args=[str(self.id)])
    def __str__(self):
        return self.name


class Package(models.Model):
    name = models.CharField(max_length=100, unique=True)
    class Meta:
        ordering = ['-name']    
    def get_absolute_url(self):
        return reverse('package-detail', args=[str(self.id)])
    def __str__(self):
        return self.name


class Rounds(models.Model):
    name = models.CharField(max_length=100, unique=True)    
    class Meta:
        ordering = ['name']
    def get_absolute_url(self):
        return reverse('rounds-detail', args=[str(self.id)])
    def __str__(self):
        return self.name


class Package_rounds(models.Model):
    package = models.ForeignKey('Package', on_delete=models.CASCADE)
    rounds = models.ForeignKey('Rounds', on_delete=models.CASCADE)
    number = models.IntegerField()
    theme = models.CharField(max_length=100, null=True, blank=True)
    class Meta:
        ordering = ['-package__name', 'number']    
    def get_absolute_url(self):
        return reverse('package_rounds-detail', args=[str(self.id)])
    def __str__(self):
        return self.package.name + ', ' + str(self.number) + '. ' + self.rounds.name


class Game(models.Model):
    date = models.DateField()
    time = models.TimeField()
    location = models.ForeignKey('Location', on_delete=models.CASCADE)
    package = models.ForeignKey('Package', on_delete=models.CASCADE)
    presenter = models.ForeignKey('Person', related_name='presenter', on_delete=models.SET_NULL, null=True, blank=True)
    photographer = models.ForeignKey('Person', related_name='photographer', on_delete=models.SET_NULL, null=True, blank=True)
    price = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    class Meta:
        ordering = ['-date']
    def get_absolute_url(self):
        return reverse('game-detail', args=[str(self.id)])
    def __str__(self):
        return str(self.date) + ', ' + self.package.name

class Application(models.Model):
    game = models.ForeignKey('Game', on_delete=models.CASCADE)
    team = models.ForeignKey('Team', on_delete=models.CASCADE)
    applicant = models.ForeignKey('Person', on_delete=models.CASCADE)
    amount = models.IntegerField()
    date = models.DateTimeField(default=now)
    sale = models.FloatField(null=True, blank=True)
    points = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    place = models.IntegerField(null=True, blank=True)
    def get_absolute_url(self):
        return reverse('application-detail', args=[str(self.id)])
    def __str__(self):
        return str(self.game.date) + ', ' + self.team.name

def application_pre_save(sender, **kwargs):
    after = kwargs['instance']
    try:
        before = Application.objects.get(id=kwargs['instance'].id)
        if before.applicant != after.applicant:
            last_team = Team.objects.exclude(application=before).filter(application__applicant=before.applicant).order_by('-application__game__date').first()
            Person.objects.filter(id=before.applicant.id).update(last_team=last_team)
    except:
        pass
    finally:
        last_team = Team.objects.filter(application__applicant=after.applicant, application__game__date__gt=after.game.date).order_by('-application__game__date').first()
        last_team = last_team or after.team
        Person.objects.filter(id=after.applicant.id).update(last_team=last_team)
   
pre_save.connect(application_pre_save, sender=Application)

'''
from django.db import connection

def get_last_result_id():
    cursor = connection.cursor()
    cursor.execute("select game_id, team_id, package_rounds_id from catalog_result order by id desc limit 1")
    row = cursor.fetchone()
    print(row[0])
    return row

def get_last_game():
    try:       
        return get_last_result_id[0]
    except:
        return None

def get_last_team():
    try:
        return get_last_result_id[0]
    except:
        return None  

def get_last_game():
    try:       
        return Game.objects.order_by(F('result__id').desc(nulls_last=True))[0].id
    except:
        return None

def get_last_team():
    try:
        return Team.objects.order_by(F('result__id').desc(nulls_last=True))[0].id
    except:
        return None        

def get_last_pr():
    try:
        pr = Package_rounds.objects.order_by(F('result__id').desc(nulls_last=True))[0]
        if pr.number < 5:
            return Package_rounds.objects.get(package=pr.package, number=pr.number+1).id
        else:
            return Package_rounds.objects.get(package=pr.package, number=1).id
    except:
        return None
'''
def get_last_game():
    return None
def get_last_team():
    return None
def get_last_pr():
    return None

class Result(models.Model):
    game = models.ForeignKey('Game', on_delete=models.CASCADE, default=get_last_game)
    team = models.ForeignKey('Team', on_delete=models.CASCADE, default=get_last_team)
    package_rounds = models.ForeignKey('Package_rounds', on_delete=models.CASCADE, default=get_last_pr)
    points = models.DecimalField(max_digits=10, decimal_places=2)
    place = models.IntegerField(null=True, blank=True)
    battle = models.ForeignKey('self', on_delete=models.SET_NULL, null=True, blank=True)
    def get_absolute_url(self):
        return reverse('result-detail', args=[str(self.id)])
    def __str__(self):
        return self.package_rounds.package.name + ', ' + str(self.package_rounds.number) + ', ' + self.team.name + ', ' + str(self.points)

'''
def result_post_save(sender, **kwargs):
    game = kwargs['instance'].game
   
post_save.connect(result_post_save, sender=Result)
'''

class Question(models.Model):
    package_rounds = models.ForeignKey('Package_rounds', on_delete=models.CASCADE)
    number = models.IntegerField()
    text = models.CharField(max_length=1000)
    correct_answer = models.CharField(max_length=100)
    value = models.DecimalField(max_digits=10, decimal_places=2)
    def get_absolute_url(self):
        return reverse('question-detail', args=[str(self.id)])
    def __str__(self):
        return self.package_rounds.package.name + ', ' + str(self.number) + '. ' + self.text


class Question_variant(models.Model):
    question = models.ForeignKey('Question', on_delete=models.CASCADE)
    number = models.CharField(max_length=1)
    text = models.CharField(max_length=100)
    def get_absolute_url(self):
        return reverse('question_variant-detail', args=[str(self.id)])
    def __str__(self):
        return self.number + '. ' + self.text


class Answer(models.Model):
    game = models.ForeignKey('Game', on_delete=models.CASCADE)
    team = models.ForeignKey('Team', on_delete=models.CASCADE)    
    question = models.ForeignKey('Question', on_delete=models.CASCADE)
    text = models.CharField(max_length=1000)
    value = models.DecimalField(max_digits=10, decimal_places=2, null=True)
    def get_absolute_url(self):
        return reverse('answer-detail', args=[str(self.id)])
    def __str__(self):
        return self.text


class Online(models.Model):
    game = models.ForeignKey('Game', on_delete=models.CASCADE)
    #package_rounds = models.ForeignKey('Package_rounds', on_delete=models.CASCADE, null=True)
    #number = models.IntegerField(null=True, blank=True)
    question = models.ForeignKey('Question', on_delete=models.SET_NULL, null=True, blank=True)
    def get_absolute_url(self):
        return reverse('online-detail', args=[str(self.id)])
    def __str__(self):
        return str(self.game.date) + ', Текущий вопрос: ' + self.question.__str__()