from django import forms

from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

import datetime #for checking renewal date range.

from .models import Person, Question_variant, Team, Game, Person
from catalog import views
    
class PhoneLoginForm(forms.Form):
    phone = forms.CharField(required=True, min_length=10, max_length=10, label='')
    phone.widget.attrs.update({'class': 'form-control'})

    def clean_phone(self):
        data = views.get_application_by_phone(self.cleaned_data['phone'])
        if not data :
            raise ValidationError(_('Не найдена регистрация команды, в которой был указан номер телефона +7' + self.cleaned_data['phone']))
        # Помните, что всегда надо возвращать "очищенные" данные.
        return data

class NaturalSelectionForm(forms.Form):
    question_variants = forms.MultipleChoiceField(
        required=True,
        widget=forms.CheckboxSelectMultiple,
        choices=(('А','А'),('Б','Б'),('В','В'),('Г','Г')),
    )

from django.contrib.sessions.backends.db import SessionStore

class NaturalSelectionForm(forms.Form):
    a = forms.BooleanField(required=False, initial=False)
    b = forms.BooleanField(required=False, initial=False)
    c = forms.BooleanField(required=False, initial=False)
    d = forms.BooleanField(required=False, initial=False)
'''
    def clean(self):
        cleaned_data = super().clean()
        if True not in self.cleaned_data.values():
            raise ValidationError(_('Необходимо выбрать вариант ответа!'))
'''

CHOICES=[('0','have not registered for the game'),
         ('1','have registered for the game')]

class VkMessagesSendForm(forms.Form):
    person = forms.ModelChoiceField(queryset=Person.objects.all(), required=False)
    team = forms.ModelChoiceField(queryset=Team.objects.all(), required=False)
    game = forms.ModelChoiceField(queryset=Game.objects.all(), required=False)
    game_choice = forms.ChoiceField(required=False, choices=CHOICES, widget=forms.Select, label='For all who', initial='0')
    text = forms.CharField(widget=forms.Textarea, required=False)

class GameResultEditForm(forms.Form):    
    team = forms.ModelChoiceField(queryset=Team.objects.all(), required=False, label='')#, disabled=True)
    round1 = forms.DecimalField(max_digits=10, decimal_places=2, required=False, label='')
    round2 = forms.DecimalField(max_digits=10, decimal_places=2, required=False, label='')
    round3 = forms.DecimalField(max_digits=10, decimal_places=2, required=False, label='')
    round4 = forms.DecimalField(max_digits=10, decimal_places=2, required=False, label='')
    round5 = forms.DecimalField(max_digits=10, decimal_places=2, required=False, label='')
    '''
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['team'].queryset = Team.objects.filter(application__game__id=kwargs['initial']['game_id'])
        print(kwargs)
    '''

class GameAppliacationAddForm(forms.Form):
    phone = forms.CharField(required=True, min_length=10, max_length=10, label='')
    phone.widget.attrs.update({'class': 'form-control'})