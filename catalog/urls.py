from django.conf.urls import url
from django.urls import path
from django.urls import re_path
from . import views

urlpatterns = [  
    re_path(r'^application/create/$', views.ApplicationCreate.as_view(), name='application_create'),
]

urlpatterns += [  
    re_path(r'^registration/$', views.GameListView.as_view(), name='registration'),
]

urlpatterns += [  
    path('', views.index, name='index'),
]

urlpatterns += [  
    re_path(r'^phone_login/$', views.phone_login, name='phone_login'),
]

urlpatterns += [  
    re_path(r'^phone_logout/$', views.phone_logout, name='phone_logout'),
]

urlpatterns += [  
    re_path(r'^natural_selection/$', views.natural_selection, name='natural_selection'),
]

urlpatterns += [  
    re_path(r'^natural_selection_result/$', views.natural_selection_result, name='natural_selection_result'),
]

urlpatterns += [  
    re_path(r'^vk_messages_send/$', views.vk_messages_send, name='vk_messages_send'),
]

urlpatterns += [  
    re_path(r'^game_list/$', views.GameListView.as_view(), name='game_list'),
]

urlpatterns += [   
    re_path(r'^game/(?P<pk>[-\w]+)/result_edit/$', views.game_result_edit, name='game_result_edit'),
]

urlpatterns += [   
    re_path(r'^game/(?P<pk>[-\w]+)/application_add/$', views.game_application_add, name='game_application_add'),
]