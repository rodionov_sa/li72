# -*- coding: utf-8 -*-
from datetime import datetime
from datetime import timedelta
import locale
import vk
import sys
import os

print(datetime.now())
#на pythonanywhere время по гринвичу, поэтому -5 часов
if datetime.now().hour > 23 - 5 or datetime.now().hour < 8 - 5:
    sys.exit()
'''
try:
    locale.setlocale(locale.LC_TIME, 'ru_RU')
except:
    locale.setlocale(locale.LC_TIME, 'RU')
'''
group_id = 'ESLPodcast72'
group_id = 'mozgoboj_tmn'
group_id = 'quizplease_tmn'
group_id = 'quizium_tmn'
group_id = 'komnatatyumen'
group_id = 'steel_character72'

group_id = os.path.join(os.path.dirname(os.path.abspath(__file__)), group_id)
users = vk.from_csv(group_id)

requests = vk.requests_get_all('friends.getRequests', {'offset': 0, 'count': 1000, 'out': 1})
print('requests:', len(requests))
friends = vk.requests_get_all('friends.get', {'offset': 0, 'count': 5000})
print('friends:', len(friends))

next_user_id = None
for u in users:
    user_id = round(float(u['id']))
    #if user_id in [1599355, 3710912, 4273990]:
    #    continue
    if user_id not in requests and user_id not in friends:
        try:
            r = vk.requests_get('users.get', {'user_ids': user_id, 'fields': 'blacklisted'})
            if 'deactivated' in r['response'][0]:
                print(user_id, 'deactivated')
                continue                        
            if r['response'][0]['blacklisted'] == 1:
                print(user_id, 'blacklisted')
                continue
        except:
            continue
        next_user_id = user_id
        break

if next_user_id is None:
    print('next_user_id not found')
    sys.exit()

next_date = datetime(2019,3,28)
next_hour = 19
next_minute = next_date.month
next_datetime = next_date + timedelta(hours=next_hour) + timedelta(minutes=next_minute)
#next_place = 'гриль-бар Колбас-Барабас'
#next_place = 'ресторан Максимилианс'
next_place = 'гриль-бар ШашлыкоFF'
next_text = next_datetime.strftime("%d %B %H:%M") + ' в ' + next_place


text = 'Привет&#128522; Лига Индиго собирает под одной крышей умных, талантливых и веселых людей vk.com/li_tyumen. '
text += 'Наши игры - отличный способ немного отвлечься от суровых рабочих будней, встряхнуть мозги и пообщаться с друзьями. '
text += 'Лига Индиго - не просто очередной квиз&#9757; Раунды очень разнообразны и каждый игрок в команде сможет проявить себя ligaindigo.ru/raunds. '
if datetime.now() > next_date:
    text += 'Следующая игра не за горами, не пропусти&#128540;'
else:
    text += 'Приходи на следующую игру ' + next_text + ' и блесни логикой, эрудицией, интуицией и чувством юмора&#128540;'

print(next_user_id, text)
r = vk.friends_add(next_user_id,text)
print(r)