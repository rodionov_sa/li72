# Generated by Django 2.1.5 on 2019-02-18 06:35

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0013_package_rounds_theme'),
    ]

    operations = [
        migrations.RenameField(
            model_name='application',
            old_name='payment',
            new_name='points',
        ),
        migrations.AddField(
            model_name='application',
            name='place',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='result',
            name='place',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='answer',
            name='application',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='catalog.Application'),
        ),
        migrations.AlterField(
            model_name='answer',
            name='question',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='catalog.Question'),
        ),
        migrations.AlterField(
            model_name='application',
            name='applicant',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='catalog.Person'),
        ),
        migrations.AlterField(
            model_name='application',
            name='game',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='catalog.Game'),
        ),
        migrations.AlterField(
            model_name='application',
            name='team',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='catalog.Team'),
        ),
        migrations.AlterField(
            model_name='game',
            name='location',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='catalog.Location'),
        ),
        migrations.AlterField(
            model_name='game',
            name='package',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='catalog.Package'),
        ),
        migrations.AlterField(
            model_name='online',
            name='game',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='catalog.Game'),
        ),
        migrations.AlterField(
            model_name='package_rounds',
            name='package',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='catalog.Package'),
        ),
        migrations.AlterField(
            model_name='package_rounds',
            name='rounds',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='catalog.Rounds'),
        ),
        migrations.AlterField(
            model_name='question',
            name='package_rounds',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='catalog.Package_rounds'),
        ),
        migrations.AlterField(
            model_name='question_variant',
            name='question',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='catalog.Question'),
        ),
        migrations.AlterField(
            model_name='result',
            name='application',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='catalog.Application'),
        ),
        migrations.AlterField(
            model_name='result',
            name='battle',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='catalog.Result'),
        ),
        migrations.AlterField(
            model_name='result',
            name='package_rounds',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='catalog.Package_rounds'),
        ),
    ]
