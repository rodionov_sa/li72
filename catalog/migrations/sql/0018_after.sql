update catalog_person p 
set last_team_id = (
	select team_id
	from catalog_application a
	join catalog_game g on g.id = a.game_id
	where a.applicant_id = p.id
	order by g.date desc
	limit 1
);