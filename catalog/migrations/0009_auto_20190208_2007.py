# Generated by Django 2.1.5 on 2019-02-08 15:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0008_auto_20190207_2316'),
    ]

    operations = [
        migrations.AlterField(
            model_name='online',
            name='question',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='catalog.Question'),
        ),
    ]
