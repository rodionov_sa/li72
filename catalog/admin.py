from django.contrib import admin
from django.db.models import F, Q, Sum, Max, Count, DecimalField
from django.db.models.functions import Coalesce
from django.conf.locale.en import formats as en_formats
	

# Register your models here.

from .models import Team, Person, Location, Package, Rounds, Package_rounds, Game, Application, Result, Question, Question_variant, Answer, Online

class Question_variantInline(admin.TabularInline):
    model = Question_variant

class Package_roundsInline(admin.TabularInline):
    model = Package_rounds

@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Team._meta.fields]
    search_fields = ['name']
    ordering = ['name']

@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Person._meta.fields]
    #list_display.append('get_team')
    search_fields = ['last_name', 'first_name']
    ordering = ['last_name', 'first_name']

@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Location._meta.fields]

@admin.register(Package)
class PackageAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Package._meta.fields]
    inlines = [Package_roundsInline]
    ordering = ['-name']

@admin.register(Rounds)
class RoundsAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Rounds._meta.fields]
    ordering = ['name']

@admin.register(Package_rounds)
class Package_roundsAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Package_rounds._meta.fields]
    list_filter = ['package__game']
    ordering = ['-package__name', 'number']

@admin.register(Game)
class GameAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Game._meta.fields]    
    ordering = ['-date']
    actions = ['calc_result']

    def calc_result(modeladmin, request, queryset):
        for game in queryset:
            for pr in Package_rounds.objects.filter(package__game=game):
                points = 1000
                place = 0
                for r in Result.objects.filter(game=game, package_rounds=pr).order_by('-points'):
                    if r.points < points:
                        place += 1
                    Result.objects.filter(id=r.id).update(place=place)
                    points = r.points
                    #print(r.team, points, place)

            place = 0
            sum_points = Coalesce(Sum('result__points', filter=Q(result__game=game)),0)
            teams = Team.objects.filter(application__game=game).annotate(sum_points=sum_points).filter(sum_points__gt=0).order_by('-sum_points')
            for t in teams:
                place += 1
                Application.objects.filter(game=game, team=t).update(points=t.sum_points, place=place)

            same_points = Application.objects.filter(
                game=game
            ).values_list(
               'points', flat=True
            ).annotate(
                count=Count('points')
            ).filter(
                count__gt=1
            )
            print(same_points.query)
            print(same_points)
            
            for sp in same_points:
                sorted_team = {}
                for pr in Package_rounds.objects.filter(package__game=game).order_by('-number'):                    
                    points = 1000
                    place = 0
                    print(pr)
                    for r in Result.objects.filter(package_rounds=pr, team__application__points=sp).order_by('-points'):
                        if r.points < points:
                            place += 1
                        sorted_team[r.team] = [place] #вроде хотел чтобы на каждую одинаковую сумму очков для каждой команды делать массив с местами в раундах
                        points = r.points
                    print(sorted_team)

    calc_result.short_description = "Calculate points and set places"

@admin.register(Application)
class ApplicationAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Application._meta.fields]
    search_fields = ['team__name', 'applicant__first_name', 'applicant__last_name']
    autocomplete_fields = ['applicant', 'team']
    list_filter = ['game']    
    #ordering = ['-game__date', '-points', 'team__name']
    ordering = ['-game__date', F('points').desc(nulls_last=True), 'team__name']

    def changelist_view(self, request, extra_context=None):
        response = super(ApplicationAdmin, self).changelist_view(request, extra_context)
        filtered_query_set = response.context_data["cl"].queryset
        amount = filtered_query_set.aggregate(total=Sum('amount'))['total']
        profit = filtered_query_set.aggregate(profit=Sum(F('amount') * F('game__price') * Coalesce(F('sale')/100,1), output_field=DecimalField(max_digits=10, decimal_places=2)))['profit']
        extra_context = {'total': str(amount) + ' participants, ' + str(profit) + ' profit'}
        response.context_data.update(extra_context)
        return response        

@admin.register(Result)
class ResultAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Result._meta.fields]
    search_fields = ['team__name', 'package_rounds__rounds__name']
    autocomplete_fields = ['team']
    list_filter = ['game']
    ordering = ['-game__date', 'team__name', 'package_rounds__number']
    save_as = True

@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Question._meta.fields]
    inlines = [Question_variantInline]
    list_filter = ['package_rounds__package__name']
    ordering = ['-package_rounds__package__name', 'number']

@admin.register(Question_variant)
class Question_variantAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Question_variant._meta.fields]
    list_filter = ['question__package_rounds__package__name']
    ordering = ['-question__package_rounds__package__name', 'question__number', 'number']

@admin.register(Answer)
class AnswerAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Answer._meta.fields]
    list_filter = ['game__package__name', 'team']
    ordering = ['-game__date', 'question__number', 'team__name']

@admin.register(Online)
class OnlineAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Online._meta.fields]