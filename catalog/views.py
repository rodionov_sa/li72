from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.urls import reverse

from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin

from django.shortcuts import get_object_or_404
from django.forms import formset_factory

from django.db.models import Q, Sum, Count
from django.db.models.functions import Coalesce

from .models import Game, Application, Person, Online, Question, Question_variant, Answer, Team, Package_rounds, Result
from .forms import PhoneLoginForm, NaturalSelectionForm, VkMessagesSendForm, GameResultEditForm, GameAppliacationAddForm




from .vk import vk

from datetime import date
       
class GameListView(LoginRequiredMixin, generic.ListView):
    model = Game

class ApplicationCreate(CreateView):
    model = Application
    fields = '__all__'

@login_required
def index(request):
    return render(request, 'index.html', {})

def get_application_by_phone(phone):
    try:
        return Application.objects.filter(applicant__phone=phone).filter(game__date__gte=date.today()).order_by('game__date')[0].id
    except:
        pass            

def phone_login(request):
    if request.session.has_key('application_id'):
        return HttpResponseRedirect(reverse('natural_selection'))
    elif request.method == 'POST':
        form = PhoneLoginForm(request.POST)
        if form.is_valid():  
            request.session['application_id'] = form.cleaned_data['phone']
            return HttpResponseRedirect(reverse('natural_selection'))
    else:
        form = PhoneLoginForm(initial={'phone': '',})
    return render(request, 'catalog/phone_login.html', {'form': form,})

def phone_logout(request):
    if request.session.has_key('application_id'):
        del request.session['application_id']
    return HttpResponseRedirect(reverse('phone_login'))

def get_hints(game, team, without_online=False):
    hints = []
    hints.append({'name': 'Служебный элемент Конец игры', 'used': False})
    hints.append({'name': 'Право на ошибку', 'used': False})
    hints.append({'name': '2 из 4', 'used': False})
    hints.append({'name': '3 из 4', 'used': False})
    hints.append({'name': '4 из 4', 'used': False})   

    question = Online.objects.get(game=game).question
    if question is None:
        return hints

    answers = Answer.objects.filter(game=game, team=team).filter(question__package_rounds=question.package_rounds).order_by('question__number')
    for a in answers:
        if len(a.text) >= 2 and len(a.text) <= 4:
            hints[len(a.text)]['used'] = True

        if without_online and a.question == question:
            continue

        if a.question.correct_answer not in a.text:            
            if hints[1]['used']:
                hints[0]['used'] = True
            else:
                hints[1]['used'] = True

    if question.package_rounds.rounds.name == 'Неестественный отбор':
        hints[0]['used'] = False

    return hints

def natural_selection(request):
    try:
        application = Application.objects.get(id=request.session['application_id'])
    except:
        if 'application_id' in request.session:
            del request.session['application_id']
        return HttpResponseRedirect(reverse('phone_login'))

    context = {'application': application, 'message': ''}
    try:    
        question = Online.objects.get(game=application.game).question
        if question is None:
            raise error
    except:
        context['message'] = 'Игра еще не началась'
        return render(request, 'catalog/natural_selection.html', context)

    context ['question'] = question
    hints = get_hints(application.game, application.team, True)
    context['hints'] = hints[1:]

    if hints[0]['used']:
        context['message'] = 'Вы ошиблись 2 раза и выбыли из игры'
        return render(request, 'catalog/natural_selection.html', context)

    if Answer.objects.filter(game=application.game, team=application.team).filter(question=question).count() > 0:
        context['message'] = 'Вы уже ответили на вопрос'
        return render(request, 'catalog/natural_selection.html', context)

    variants = Question_variant.objects.filter(question=question).order_by('number')

    if request.method == 'POST':
        form = NaturalSelectionForm(request.POST)
        form.is_valid()
        text = ''
        if form.cleaned_data['a']:
            text += variants[0].number
        if form.cleaned_data['b']:
            text += variants[1].number
        if form.cleaned_data['c']:
            text += variants[2].number
        if form.cleaned_data['d']:
            text += variants[3].number     

        if len(text) == 0:
            context['message'] = 'Вы не указали вариант ответа'
            context['form'] = form           
        elif len(text) > 1 and hints[len(text)]['used']:
            context['message'] = 'Вы уже использовали подсказку ' + hints[len(text)]['name']
            context['form'] = form
        #print(list(form.cleaned_data.values()).count(True))
        if context['message'] == '':
            value = question.value            
            if len(text) > 1:
                #если неестественный отбор, то пересчитаем стоимость ответа:
                if question.package_rounds.rounds.name == 'Неестественный отбор':
                    value = value - value/4*(len(text) - 1)                
                hints[len(text)]['used'] = True
                context['hints'] = hints[1:]
            if question.correct_answer not in text:
                value = 0
            answer = Answer(game=application.game, team=application.team, question=question, text=text, value = value)
            answer.save()
            context['message'] = 'Ответ принят'
            
            ########## автопереход на следующий вопрос
            '''
            try:
                question = Question.objects.filter(package_rounds=question.package_rounds).filter(number__gt=question.number).order_by('number')[0]
                Online.objects.filter(game=application.game).update(question=question)
            except:
                pass
            '''
            ##########

    else:
        form = NaturalSelectionForm()
        context['form'] = form
    
    #form.fields['question_variants'].choices =  [(x.number, x.number + '. ' + x.text) for x in variants]
    form.fields['a'].label = variants[0].__str__()
    form.fields['b'].label = variants[1].__str__()
    form.fields['c'].label = variants[2].__str__()
    form.fields['d'].label = variants[3].__str__()    
    return render(request, 'catalog/natural_selection.html', context)


@login_required
def natural_selection_result(request):
    try:
        online = Online.objects.filter(game__date__lte=date.today()).order_by('-game__date').first()
        question = online.question
    except:
        raise Http404("No online games")

    if question is not None:
        sum_round = Coalesce(Sum('answer__value', filter=Q(answer__game=online.game, answer__question__package_rounds=question.package_rounds)),0)
        teams = Team.objects.filter(application__game=online.game, application__amount__gt=0).annotate(sum_round=sum_round).order_by('-sum_round', 'name')
    else:
        teams = Team.objects.filter(application__game=online.game, application__amount__gt=0).annotate(sum_round=Sum('id')*0).order_by('name')

    first = True
    header = ['Команда']
    rows = []
    
    for t in teams:
        row = {}
        row['meta'] = {}
        row['data'] = []
        hints = get_hints(online.game, t)
        row['meta']['strikeout'] = hints[0]['used']
        row['data'].append(t.name)
        for h in hints[1:]:
            if first:
                header.append(h['name'])            
            if h['used']:
                row['data'].append('X')
            else:
                row['data'].append('')
        
        if question is not None:
            for q in Question.objects.filter(package_rounds=question.package_rounds).order_by('number'):
                if first:
                    header.append(q.number)
                try:
                    value = Answer.objects.get(game=online.game, team=t, question=q).value
                    row['data'].append(round(value,0))
                except:
                    row['data'].append('')

        first = False
        row['data'].append(round(t.sum_round,0))
        rows.append(row)
    header.append('Баллы')

    context = {'header': header, 'rows': rows}
    return render(request, 'catalog/natural_selection_result.html', context)


@login_required
def vk_messages_send(request):
    if request.method == 'POST':
        form = VkMessagesSendForm(request.POST)
        if form.is_valid():
            person = form.cleaned_data['person']
            team = form.cleaned_data['team']
            game = form.cleaned_data['game']
            game_choice = form.cleaned_data['game_choice']
            text = form.cleaned_data['text']
            
            persons = Person.objects.none()
            if person is not None:
                persons = Person.objects.filter(id=person.id)
            
            if game is not None:
                teams = Team.objects.filter(application__game=game)
                if game_choice == '0':                    
                    participants = Person.objects.exclude(last_team__in=teams)
                elif game_choice == '1':
                    participants = Person.objects.filter(last_team__in=teams)
                if persons.exists() and participants.exists():
                    persons = persons.intersection(participants)
                else:
                    persons = persons.union(participants)
            if team is not None:
                participants = Person.objects.filter(last_team=team)
                if persons.exists() and participants.exists():
                    persons = persons.intersection(participants)
                else:
                    persons = persons.union(participants)

            for p in persons.values('first_name', 'last_name', 'vk_id', 'last_team__name'):
                if p['vk_id'] is None or p['last_team__name'] is None:
                    continue
                message = text
                for k, v in p.items():
                    message = message.replace('%' + k + '%', str(v))
                #print(p)
                #print(message)
                vk.messages_send(p['vk_id'],message,'')

    else:
        form = VkMessagesSendForm(initial={'text': r'Добрый день, %first_name%!"Лига Индиго Тюмень" приветствует команду "%last_team__name%"'})
        
    context = {}
    context['form'] = form
    return render(request, 'catalog/vk_messages_send.html', context)


@login_required
def game_result_edit(request, pk):
    game = get_object_or_404(Game, pk = pk)        
    GameResultEditFormSet = formset_factory(GameResultEditForm, extra=0)

    if request.method == 'POST':
        formset = GameResultEditFormSet(request.POST)        
        if formset.is_valid():
            for data in formset.cleaned_data:
                for k, v in data.items():
                    if k[0:5] == 'round':
                        number = int(k[5:])
                        package_rounds = Package_rounds.objects.get(package=game.package, number=number)
                        try:
                            result = Result.objects.get(game=game, team=data['team'], package_rounds=package_rounds)
                            if v is not None and v != result.points:
                                result.points = v
                                result.save()
                            elif v is None:
                                result.delete()
                        except:
                            if v is not None:
                                result = Result(game=game, team=data['team'], package_rounds=package_rounds, points=v)
                                result.save()
            return HttpResponseRedirect(reverse('game_list'))
    else:        
        initial = []
        for app in Application.objects.filter(game=game, amount__gt=0).order_by('team__name'):
            result = {}
            result['team'] = app.team
            for pr in Package_rounds.objects.filter(package=game.package):
                try:
                    points = Result.objects.get(game=game, team=app.team, package_rounds=pr).points
                except:
                    points = None
                result['round' + str(pr.number)] = points
            initial.append(result)        
        formset = GameResultEditFormSet(initial=initial)

    return render(request, 'catalog/game_result_edit.html', {'formset': formset})

@login_required
def game_application_add(request, pk):
    if request.method == 'POST':
        form = GameAppliacationAddForm(request.POST)
    else:
        form = GameAppliacationAddForm(initial={})
    return render(request, 'catalog/game_application_add.html', {'form': form})