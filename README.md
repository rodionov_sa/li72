# li72

heroku run python manage.py migrate

#export from local db to file
pg_dump -U postgres li72 > li72.sql

#import from file to local db
psql -U postgres -c "drop database li72"
psql -U postgres -c "create database li72"
psql -U postgres li72 < li72.sql

#set variables for heroku
SET PGUSER=postgres 
SET PGPASSWORD=

#import from heroku db to local db
heroku pg:pull DATABASE_URL li72 --app li72

#export from local db to heroku db
heroku pg:reset DATABASE_URL --app li72
heroku pg:push li72 DATABASE_URL --app li72


#heroku create and download backup to latest.dump
heroku pg:backups:capture
heroku pg:backups:download

#import from dump file to local db
pg_restore --verbose --clean --no-acl --no-owner -h localhost -U postgres -d li72 latest.dump